# README #

This repository is a stack of common [docker](https://www.docker.com/whatisdocker) containers, organized on top of docker [compose](https://docs.docker.com/compose/).

## What is this repository for? ##

* Having common docker services into a docker-compose.yml file, which will serve as base to create your own docker environments.
* Sensitive data (as DB passwords) is saved with fake data into a repository file (.<app>.env.repo), which you need to copy (as .<app>.env) and edit as your needs, while the .env file will no be saved into the repository.

## Actual containers ##
* [mysql (official)](https://hub.docker.com/_/mysql/).
* [redmine (official)](https://hub.docker.com/_/redmine/)

## How do I get set up? ##

### Summary of set up ###
* Clone this repository.
* Copy .<app>.env.repo file to .<app>.env and edit it to your needs.
* Start up your containers.

### Detailed setup ###
* Clone this repository:

        git clone https://henderb@bitbucket.org/henderb/compose-stack.git

* Copy .<app>.env.repo file to .<app>.env and **edit it to your needs**. For example, the file .mysql.env.repo must be copied to .mysql.env and this last file must be edited with your data, and this data is protected with a .gitignore file, which will avoid to put those .env files into the git repository.

        cp .mysql.env.repo .mysql.env
        vim .mysql.env

* Start up your containers:

        docker-compose up mysql
        docker-compose up redmine

    + To start everything:

            docker-compose up

### Dependencies ###
* You must have docker [installed](https://docs.docker.com/installation/) on your host machine.
* You must have docker compose [installed](https://docs.docker.com/compose/install/) on your host.
* Open on your host the ports you need to be word reachable.

## How to test ##
* Start mysql container like described above.
* Start redmine container.
* Point a browser to http://your-server:3000.
* Use admin/admin as credentials to login.


## Contribution guidelines ##

* Writing tests.
* Code review.
* Informing about issues.

## Who do I talk to? ##

* Repo owner or admin.